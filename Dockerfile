# base
FROM httpd:2.4.52-alpine

# copy static files
COPY ./public/ /usr/local/apache2/htdocs/

RUN chmod -R a+r /usr/local/apache2/htdocs/images/
RUN ls -la /usr/local/apache2/htdocs

COPY ./apache/.htaccess /usr/local/apache2/htdocs/.htaccess
COPY ./apache/server.conf /usr/local/apache2/conf/httpd.conf
